<?php
/**
 * Created by Touqeer Shafi.
 * email: touqeer.shafi@gmail.com
 * Date: 9/24/2018
 * Time: 8:05 PM
 */

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TrendController extends Controller
{

    public function fetchTwitterTrends(Request $request, \TwitterAPIExchange $twitterAPIExchange) {

        $trends = Cache::remember('trends_' . $request->get('id'),  Carbon::now()->addMinutes(2), function() use ($request, $twitterAPIExchange) {
            $trends = $twitterAPIExchange->setGetfield(http_build_query(['id' => $request->get('id')]))
                ->buildOauth('https://api.twitter.com/1.1/trends/place.json', "GET")
                ->performRequest();

            return json_decode($trends, true);
        });

        if(isset($trends['errors'])) {
            return response()->json(['status' => false] + ['error' => $trends['errors'][0]['message']]);
        }
        
        return response()->json(['status' => true, 'data' => $trends[0]]);

    }

}


/*
 * End of TrendController.php
 */