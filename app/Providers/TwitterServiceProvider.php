<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class TwitterServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(\TwitterAPIExchange::class, function ($app) {
            return new \TwitterAPIExchange([
                'oauth_access_token' => env("OAUTH_ACCESS_TOKEN"),
                'oauth_access_token_secret' => env("OAUTH_ACCESS_TOKEN_SECRET"),
                'consumer_key' => env("CONSUMER_KEY"),
                'consumer_secret' => env("CONSUMER_SECRET")
            ]);
        });
    }

}
