# Twitter Trend Backend Lumen

## Configuration

#### Change .env file it has following env variables for twitter configurations:

1. OAUTH_ACCESS_TOKEN
2. OAUTH_ACCESS_TOKEN_SECRET
3. CONSUMER_KEY
4. CONSUMER_SECRET

#### Cache 
Due to twitter API Quota i'have used cache on the backend side which you can find in this file

`\app\Http\Controllers\TrendController.php`

Right now it's caching tweets for 2 minutes for each location using carbon

`Carbon::now()->addMinutes(2)`

#### Install Dependencies by using composer

`composer install`